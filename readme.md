# OmekaS Workshop
### by Anna Neatrour

Omeka is a content management system (CMS) specifically built for digital exhibits, it is widely used by the GLAM (Galleries, Libraries, Archives, and Museums) Community. It also has great documentation and projects to draw upon for inspiration.

* Learn how to present information on the web
* Learn how to make your own websites and additional skills like HTML markup. This can be valuable practice if you ever need to write for the web or learn a Content Management System (CMS) for managing web content in the future
* How to shape your narrative to a particular digital tool
* Develop information for a broad audience

## Workshop Requirements and Technical Information

* The workshop will have some time set aside for you to create a test exhibit on the [Marriott Library's Training OmekaS](https://omekastraining.lib.utah.edu/) site
* I recommend bringing a laptop to the workshop, so you can fully participate
* If you don't already have a user account by signing up in advance of the workshop, please let me know
* Your account on the OmekaS sandbox site will be deleted after two weeks
* The morning of the workshop (Feb 10, 2022), please download this gitlab repository, so sample images will be saved on your computer

## Workshop Topics

* Omeka Ecosystem
* Hosting options
* Working with students on digital exhibits
* OmekaS interface
* Exhibit authoring
* Upload an item, add metadata, create a basic exhibit (in groups)


## Repository Contents

* Workshop presentation slides
* Sample items and metadata for creating a sandbox digital exhibit

## Workshop Activities

* After downloading this repository, you will see a set of folders with images and descriptive information (or metadata!)
* Use these files and descriptive information to work in groups and start building a basic exhibit.

### Exhibit Building Activities

1. Working in small groups or individually, upload at least 3 items, including some descriptive information or metadata
2. Create 3-4 pages for your assigned site. You can use a [lorem ipsum generator](https://www.lipsum.com/) as a placeholder for paragraphs of text. 
3. Experiment with formatting your text on the page, including adding a bulleted list and some links. 
4. Review the navigation for your site, add pages to the navigation and reorder them
5. Add a visual element to your web page by embedding images you have uploaded. Experiment with different options.
