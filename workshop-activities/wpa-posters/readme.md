# WPA Book Posters

[https://www.loc.gov/item/2011645392/](https://www.loc.gov/item/2011645392/)

* Title: Be kind to books club Are you a member? / / Gregg.
* Summary: WPA poster shows a group of children holding a sign for the book club.
* Contributor Names: Gregg, Arlington, artist; United States. Works Progress Administration (Ill.), sponsor
* Created / Published: Chicago : Made by Illinois W.P.A. Art Project, [between 1936 and 1940]
* Subject Headings: Books--Illinois--1930-1940; Children--Illinois--1930-1940; Reading--Illinois--1930-1940


[https://www.loc.gov/item/98507722/](https://www.loc.gov/item/98507722/)

* Title: In March read the books you've always meant to read
* Summary: Poster for statewide Library Project showing a windblown woman and books by authors such as Scott, Dumas, Thackeray, Dickens, Austen, and others.
* Contributor Names: Federal Art Project, sponsor
* Created / Published: Chicago : Ill. Art Proj., [between 1936 and 1941]
* Subject Headings: Reading--Illinois--1930-1950; Books--1930-1950; Libraries--Illinois--1930-1950

[https://www.loc.gov/item/2011645391/](https://www.loc.gov/item/2011645391/)

* Title: Don't gum up a book
* Summary: WPA poster shows a boy walking on a book with gummed up feet and hands.
* Contributor Names: Gregg, Arlington, artist; United States. Works Progress Administration (Ill.), sponsor
* Created / Published: Chicago : Made by Illinois W.P.A. Art Project, [between 1936 and 1940]
* Subject Headings: Books--1930-1940; Conservation & restoration--1930-1940

[https://www.loc.gov/item/2011645388/](https://www.loc.gov/item/2011645388/)

* Title: This breaks the back of a book!
* Summary: WPA poster shows a boy doing a handstand on the spine of a book.
* Contributor Names: Gregg, Arlington, artist; United States. Works Progress Administration (Ill.), sponsor
* Created / Published: Chicago : Made by Illinois W.P.A. Art Project, [between 1936 and 1940]
* Subject Headings: Books--1930-1940; Conservation & restoration--1930-1940

[https://www.loc.gov/item/98517816/](https://www.loc.gov/item/98517816/)

* Title: Passports to adventure
* Summary: Poster promoting reading as an avenue to adventure, showing a knight in armor and fleur-de-lis.
* Created / Published: Ill. : Federal Art Project, WPA, [between 1936 and 1939]
* Subject Headings: Reading--Illinois--1930-1950; Books--1930-1940; Knights--1930-1940; Fleur-de-lis--1930-1940


[https://www.loc.gov/item/98510133/](https://www.loc.gov/item/98510133/)

* Title: January--A year of good reading ahead / Hazlett.
* Summary: Poster for statewide WPA Library Project, Illinois, showing a child pulling a sled full of books toward a house.
* Contributor Names: Federal Art Project, sponsor
* Created / Published: Chicago : Illinois WPA Art Project, [between 1936 and 1941]
* Subject Headings: Books--1930-1950; Children playing in snow--Illinois--1930-1950; Sleds & sleighs--Illinois--1930-1950
