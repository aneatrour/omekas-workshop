# Althea Gibson Photo Information


[https://commons.wikimedia.org/wiki/File:Althea-Gibson-Queen-Elizabeth-Wimbledon-1957.jpg](https://commons.wikimedia.org/wiki/File:Althea-Gibson-Queen-Elizabeth-Wimbledon-1957.jpg)

Photograph of Queen Elizabeth II presenting Althea Gibson with the Venus Rosewater Trophy at the 1957 Wimbledon Women's Singles Championships. Gibson defeated Darlene Hard (left), her doubles partner; Hard and Gibson were the 1957 Wimbledon Women's Doubles Champions.

Caption reads as follows:  
ROYAL HAND FOR CHAMPION—Smiling Queen Elizabeth II presents the championship trophy—a large gold plate—to Althea Gibson of New York (center), who won the women's singles tennis title Saturday at Wimbledon, England. Looking on at left is Darlene Hard of Montebello, Calif., defeated by Miss Gibson in the title match.  
Date:	6 July 1957 event date  


[https://commons.wikimedia.org/wiki/File:Althea-Gibson-Darlene-Hard-Wimbledon-1957.jpg](https://commons.wikimedia.org/wiki/File:Althea-Gibson-Darlene-Hard-Wimbledon-1957.jpg)

Photograph of Darlene Hard congratulating Althea Gibson upon her winning the 1957 Wimbledon Women's Singles Championship. Hard and Gibson were doubles partners, and were the 1957 Wimbledon Women's Doubles Champions.

Caption reads as follows:  
GOOD LOSER—Darlene Hard of California kisses Althea Gibson of New York, after the Harlemite defeated her 6–2, 6–3 in the Women's Singles Tennis Championship at Wimbledon. Miss Gibson, first of her race to win the title, holds the trophy presented to her by Queen Elizabeth II. After returning to her native New York, the former Harlem paddle tennis star was afforded the traditional ticker tape welcome.  
Date:	6 July 1957 (photo date)  

[https://commons.wikimedia.org/wiki/File:Althea_Gibson_statue.jpg](https://commons.wikimedia.org/wiki/File:Althea_Gibson_statue.jpg)

English: Statue of Althea Gibson, north of Franklin Street in Branch Brook Park, Newark, NJ, USA  
Date:	5 March 2013, 12:43:39  
Source:	Thomas Jay Warren; Own work  
Permissions: For the image, CC-BY-SA-3.0 as own work of the uploader   

[https://commons.wikimedia.org/wiki/File:Althea_Gibson_NYWTS.jpg](https://commons.wikimedia.org/wiki/File:Althea_Gibson_NYWTS.jpg)

Description: Althea Gibson, half-length portrait, holding tennis racquet / World Telegram & Sun photo by Fred Palumbo.  

Date: 1956  
Source:	Library of Congress Prints and Photographs Division. New York World-Telegram and the Sun Newspaper Photograph Collection. http://hdl.loc.gov/loc.pnp/cph.3c14745  
Author:	New York World-Telegram and the Sun staff photographer: Palumbo, Fred, photographer.  

[https://commons.wikimedia.org/wiki/File:Althea_Gibson_-_El_Grafico_2033-crop.jpg](https://commons.wikimedia.org/wiki/File:Althea_Gibson_-_El_Grafico_2033-crop.jpg)

Description: Althea Gibson, Wimbledon women's champion, on the cover of El Gráfico for August 29, 1958. Edition 2033.  

Date:	29 August 1958  