# Skiing in Utah

[https://collections.lib.utah.edu/details?id=938663](https://collections.lib.utah.edu/details?id=938663)

* Title: Steve Burton, Brighton Ski School instructor wearing a hat.
* Description: Black and white photograph of Steve Burton, Brighton Ski School instructor, taken in the 1960s
* Subject: Ski resorts--Employees--Utah--Photographs; Skiers--Photographs; Skis and skiing--Utah--Photographs; Ski resorts--Utah--Photographs; Burton, Steve--Photographs
* Date: 1960; 1961; 1962; 1963; 1964; 1965; 1966; 1967; 1968; 1969

[https://collections.lib.utah.edu/details?id=945181](https://collections.lib.utah.edu/details?id=945181)

* Title: Alex Von Wichman, 1985
* Description: Photo of Alex Von Wichman of the University of Utah Ski Team ski racing in 1985.
* Subject: University of Utah. Ski team--Photographs; Skiers--Photographs; Ski racing--Photographs; Skis and skiing--Utah--Photographs
* Date: 1985

[https://collections.lib.utah.edu/details?id=938634](https://collections.lib.utah.edu/details?id=938634)

* Title: Ski racer descending a slope while onlookers watch.
* Description: Photograph of a ski racer descending a slope while others watching, Utah ski resort, taken in 1960s
* Subject: Skiers--Photographs; Skis and skiing--Utah--Photographs; Ski resorts--Utah--Photographs; Ski racing--Photographs
* Date: 1960; 1961; 1962; 1963; 1964; 1965; 1966; 1967; 1968; 1969

[https://collections.lib.utah.edu/details?id=959919](https://collections.lib.utah.edu/details?id=959919)

* Title: Alf Engen ski jumping.
* Description: Photo of Alf Engen ski jumping
* Subject: Engen, Alf, 1909-1997--Photographs; Skiers--Photographs; Ski jumping--Photographs
* Date: 1940; 1941; 1942; 1943; 1944; 1945; 1946; 1947; 1948; 1949; 1950

[https://collections.lib.utah.edu/details?id=948074](https://collections.lib.utah.edu/details?id=948074)


* Title: Difficult synchronized ski routine being performed by the 1975 Carnival cast - Park Smalley, Kathy Brook, TimMcFerron, Mark Stiegemeier, Penelope Street, and Robert Young.
* Description	Photo of a difficult synchronized ski routine being performed by the 1975 Carnival cast - Park Smalley, Kathy Brook, TimMcFerron, Mark Stiegemeier, Penelope Street, and Robert Young
* Subject: Stiegemeier, Mark, 1953- --Photographs; Freestyle skiing--Photographs; Skiers--Photographs
* Date: 1975

[https://collections.lib.utah.edu/details?id=977752](https://collections.lib.utah.edu/details?id=977752)

* Title: Pro-Utah, Inc. Photograph Collection; Skiers
* Subject: Universities & colleges; Travel; Recreation; Education; Tourism; Refineries; Skiing; Skiers

[https://collections.lib.utah.edu/details?id=945010](https://collections.lib.utah.edu/details?id=945010)

* Title: 1985 Alpine team
* Description: Group photo of the University of Utah Alpine Ski Team in 1985
* Subject: University of Utah. Ski team--Photographs; Skiers--Photographs; Ski racing--Photographs; Skis and skiing--Utah--Photographs
* Date: 1985

[https://collections.lib.utah.edu/details?id=942258](https://collections.lib.utah.edu/details?id=942258)

* Title:	Unidentified airborne ski jumper.
* Description: Photo of an Unidentified airborne ski jumper
* Subject: Skiers--Utah--Photographs
* Date: 1946; 1947; 1948; 1949; 1950; 1951; 1952; 1953; 1954; 1955; 1956; 1957; 1958; 1959; 1960
