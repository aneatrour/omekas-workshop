# Historical Cats


[https://www.europeana.eu/portal/en/record/9200579/c4dhevku.html](https://www.europeana.eu/portal/en/record/9200579/c4dhevku.html)

Title: Mummified cat, ancient Egypt, 2000-100 BCE  
Cats in ancient Egypt were considered sacred and were connected with Bastet, the goddess of fertility, protection and pleasure. Bastet was depicted as a woman with the head of a cat or lioness. Cats were worshipped in her honour by people at home and in temples and were mummified along with mice and bowls of milk to feed them in the afterlife. The mummification process was similar to human mummification. The cats were normally placed in a sitting position and the tightly bound cloths were painted with feline features.  
maker: Unknown maker  
Place made: Egypt  
Created by: Science Museum, London  

[https://www.europeana.eu/portal/record/9200579/njxzg2hh.html](https://www.europeana.eu/portal/record/9200579/njxzg2hh.html)

A cat is watching as a couple sort out their store and the ducks, fish and vegetables they are selling.  
Engraving by Burnet after Mieris - https://www.europeana.eu/portal/record/9200579/njxzg2hh.html.  
Wellcome Collection - https://wellcomecollection.org/works/njxzg2hh.  
CC BY - http://creativecommons.org/licenses/by/4.0/  

[https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126284726.html](https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126284726.html)  
Cats and mice from BL Royal 12 C XIX, f. 36v  
The British Library - http://www.bl.uk/catalogues/illuminatedmanuscripts/ILLUMIN.ASP?Size=mid&IllID=55668.  
Public Domain - http://creativecommons.org/publicdomain/mark/1.0/  

[https://www.europeana.eu/portal/record/9200579/va4ggdqk.html](https://www.europeana.eu/portal/record/9200579/va4ggdqk.html)  
Four mice, a cat and a dog. Cut-out engraving pasted onto paper, 16--?    
Wellcome Collection - https://wellcomecollection.org/works/va4ggdqk.  
CC BY - http://creativecommons.org/licenses/by/4.0/  

[https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126284167.html](https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126284167.html)  
Cat playing a rebec from BL Harley 6563, f. 40  
The British Library - http://www.bl.uk/catalogues/illuminatedmanuscripts/ILLUMIN.ASP?Size=mid&IllID=28879  
Public Domain - http://creativecommons.org/publicdomain/mark/1.0/  

[https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126261093.html](https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126261093.html)  
Cat and mouse from BL Sloane 4016, f. 40  
The British Library - http://www.bl.uk/catalogues/illuminatedmanuscripts/ILLUMIN.ASP?Size=mid&IllID=49468.  
Public Domain - http://creativecommons.org/publicdomain/mark/1.0/  

[https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126268815.html](https://www.europeana.eu/portal/record/9200397/BibliographicResource_3000126268815.html)  
Cat in a tower from BL Harley 6563, f. 72  
The British Library - http://www.bl.uk/catalogues/illuminatedmanuscripts/ILLUMIN.ASP?Size=mid&IllID=23227.  
Public Domain - http://creativecommons.org/publicdomain/mark/1.0/  

[https://www.europeana.eu/portal/record/90402/RP_T_1952_114.html](https://www.europeana.eu/portal/record/90402/RP_T_1952_114.html)  
(en) Six Studies of a Cat; (nl) Zes studies van een kat  
Rijksmuseum - http://hdl.handle.net/10934/RM0001.collect.26890.   
Public Domain - http://creativecommons.org/publicdomain/mark/1.0/  
