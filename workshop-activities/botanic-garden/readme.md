# Botanic Garden

## Botanic Garden Volume 1

* Title:	Botanic Garden, v. 1
* Subject:	Blake, William, 1757-1827; Botany -- Poetry; Engravings; Fuselli, Henri
* Creator:	Erasmus Darwin (1731-1802)
* Description:	Darwin, Erasmus (1731-1802). The botanic garden. London: Printed for J. Johnson, 1795 Third edition PR3396 A7 1795
* Publisher:	Digitized by J. Willard Marriott Library, University of Utah
* Date:	1795
* Type:	Text
* Rights Management:	http://rightsstatements.org/vocab/NoC-US/1.0/
* Reference URL	https://collections.lib.utah.edu/ark:/87278/s69p4fzn

## Botanic Garden Volume 2

* Title:	Botanic Garden, v. 2
* Subject:	Blake, William, 1757-1827; Botany -- Poetry; Engravings; Fuselli, Henri
* Creator:	Erasmus Darwin (1731-1802)
* Description:	Darwin, Erasmus (1731-1802). The botanic garden. London: Printed for J. Johnson, 1795 Third edition PR3396 A7 1795
* Publisher:	Digitized by J. Willard Marriott Library, University of Utah
* Date:	1795
* Type:	Text
* Rights Management:	http://rightsstatements.org/vocab/NoC-US/1.0/
* Reference URL:	https://collections.lib.utah.edu/ark:/87278/s6th9drc