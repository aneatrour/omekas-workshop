# Web of Mystery

[Ace Magazines article on Wikipedia](https://en.wikipedia.org/wiki/A._A._Wyn%27s_Magazine_Publishers)   
[Web of Mystery on Grand Comics Database](https://www.comics.org/series/12402/)  

* [Web of Mystery Issue 02](https://archive.org/details/webofmystery-comics-02): The Lorelei of Loon Lake; Legacy of the Accused
* [Web of Mystery Issue 04](https://archive.org/details/webofmystery-comics-04): Stalked by a Nameless Dread, pencils by Jim McLaughlin; Vengeance of the Undead; Sign of the Smiling Spectre; The Haunted House, pencils by Gene Colan
* [Web of Mystery Issue 21](https://archive.org/details/webofmystery-comics-21): Ghoul's Gold; Meet the Cowled Lady, pencils by Chic Stone; Queen of the Vampires
* [Web of Mystery Issue 26](https://archive.org/details/webofmystery-comics-26): Spell of the She-Bat, pencils by Jim McLaughlin; Satan's Impresario, pencils by JimMcLaughlin
* [Web of Mystery Issue 27](https://archive.org/details/webofmystery-comics-27): One Door from Disaster, pencils and inks by Lou Cameron; Valley of the Scaly Monsters, pencils and inks by "Ace" Baker
